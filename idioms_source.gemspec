# frozen_string_literal: true

require 'json'

$LOAD_PATH.push File.expand_path('lib', __dir__)

require 'idioms_source/version'

Gem::Specification.new do |s|
  s.name        = 'idioms_source'
  s.version     = IdiomsSource::VERSION
  s.authors     = ['Marco Roth']
  s.email       = ['marco.roth@intergga.ch']
  s.homepage    = 'https://glancr.de/sources/idioms_source'
  s.summary     = 'Default Idioms mirr.OS data source'
  s.description = 'A data source with a set of random idioms for your mirror'
  s.license     = 'MIT'
  s.metadata    = {
    'json' => {
      type: 'sources',
      title: {
        enGb: 'Default Idioms',
        deDe: 'Standard Sprüche'
      },
      description: {
        enGb: s.description,
        deDe: 'Eine Datenquelle mit einer Menge an Zitaten für deinen Spiegel'
      },
      groups: ['idiom_collection']
    }.to_json
  }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'README.md']
  s.add_development_dependency 'rails', '~> 5.2'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rubocop-rails'
end
