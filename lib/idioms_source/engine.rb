# frozen_string_literal: true

module IdiomsSource
  class Engine < ::Rails::Engine
    isolate_namespace IdiomsSource
    config.generators.api_only = true
  end
end
